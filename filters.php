<?php
/**
 * Kindling Support - Filter.
 *
 * @package Kindling_Support
 *
*/

if (!function_exists('add_action')) {
    return;
}


add_action('kindling_ready', function () {
    /**
     * Sets navigation menu item class.
     */
    add_filter('nav_menu_css_class', function ($classes, $item) {
        $classes = collect(
            is_string($classes) ? explode($classes, ' ') : $classes
        );

        $active = [
            'current_page_item',
            'current-menu-ancestor',
            'current_page_ancestor',
            'current-menu-item',
            'current_page_parent',
            'current-menu-parent',
        ];

        $dropdown = [
            'menu-item-has-children',
        ];

        if ($classes->intersect($active)->count() > 0) {
            $classes->push('active');
        }

        if ($classes->intersect($dropdown)->count() > 0) {
            $classes->push('dropdown');
        }

        $result = $classes->unique()
			->diff( array_merge( $active, $dropdown ) )
			->map( 'trim' )
			->reject( function ($value) {
				foreach ( [ 'menu-item-', 'page_item', 'page-item' ] as $needle ) {
					if ( str_contains( $value, $needle ) ) {
						return true;
					}
				}
				return false;
			} );

		if ( method_exists( $result, 'all' ) ) {
			$result = $result->all();
		}

		return $result;
    }, 10, 2);

    /**
     * Allow SVG Uploads
     */
    add_filter('upload_mimes', function ($mimes) {
        $mimes['svg'] = 'image/svg+xml';

        return $mimes;
    });

    /**
     * Enable Gravity Forms visibility settings.
     */
    add_filter('gform_enable_field_label_visibility_settings', '__return_true');

    /**
     * Add <body> classes
     */
    add_filter('body_class', function ($classes) {
        // Add page slug if it doesn't exist
        if (is_single() || is_page() && !is_front_page()) {
            if (!in_array(basename(get_permalink()), $classes)) {
                $classes[] = basename(get_permalink());
            }
        }

        return $classes;
    });

    /**
     * Handles removing the hicpo_pre_get_posts filter.
     *
     * In the Intuitive Custom Post Order there is a template filter
     * that forces orderby=menu_order and order=ASC on active post types.
     * This causes issues when you do not want the menu_order behavior, such
     * as when you want a random order.
     *
     * @link    https://wordpress.org/plugins/intuitive-custom-post-order/
     *
     * @param   object  $query  The current query to filter.
     *
     * @return  object          The filtered query.
     */
    add_filter('pre_get_posts', function ($query) {
        global $wp_filter;

        if (!isset($wp_filter['pre_get_posts']) or is_admin()) {
            return $query;
        } // if()

        $pre_get_posts_filters = $wp_filter['pre_get_posts'];

        foreach ($pre_get_posts_filters as $filters_key => $filters) {
            foreach ($filters as $filter_key => $filter) {
                $correct_filter = (strpos($filter_key, 'hicpo_pre_get_posts') !== false);
                $has_function = isset($filter['function']);

                if ($correct_filter and $has_function) {
                    remove_filter('pre_get_posts', $filter['function'], $filters_key);
                } // if()
            } // foreach()
        } // foreach()


        return $query;
    }, 0);

    /**
     * Removes current parent classes for navigation items without children.
     */
    add_filter('wp_nav_menu_objects', function ($sorted_menu_items, $args) {
        if ($args->depth > 1) {
            return $sorted_menu_items;
        }

        return collect($sorted_menu_items)->map(function ($item) {
            $item->classes = collect($item->classes)
                ->reject(function ($class) {
                    return $class === 'current_page_parent';
                })->toArray();

            return $item;
        })->toArray();
    }, 1, 2);
});
