<?php

namespace Kindling\Support\NavWalkers;

class PrimaryNavWalker extends \Walker_Nav_Menu
{
    /**
     * Start the element output.
     *
     * The $args parameter holds additional values that may be used with the child
     * class methods. Includes the element output also.
     *
     * @since 2.1.0
     *
     * @param string $output            Used to append additional content (passed by reference).
     * @param object $object            The data object.
     * @param int    $depth             Depth of the item.
     * @param array  $args              An array of additional arguments.
     * @param int    $current_object_id ID of the current item.
     */
    public function start_el(&$output, $object, $depth = 0, $args = [], $current_object_id = 0)
    {
        // phpcs:enable
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';

        $classes = empty($object->classes) ? [] : (array)$object->classes;
        $classes = apply_filters('nav_menu_css_class', array_filter($classes), $object, $args);

        // Convert Collection to an array if necessary
        if ($classes instanceof \Illuminate\Support\Collection) {
            $classes = $classes->toArray();
        }

        $class_names = join(' ', $classes);
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $current_object_id = apply_filters('nav_menu_item_id', 'menu-item-' . $object->ID, $object, $args);
        $current_object_id = $current_object_id ? ' id="' . esc_attr($current_object_id) . '"' : '';

        $output .= $indent . '<li' . $current_object_id . $value . $class_names . '>';

        $atts = [];
        $atts['title'] = !empty($object->title) ? $object->title : '';
        $atts['target'] = !empty($object->target) ? $object->target : '';
        $atts['rel'] = !empty($object->xfn) ? $object->xfn : '';
        $atts['href'] = !empty($object->url) ? $object->url : '';

        $atts = apply_filters('nav_menu_link_attributes', $atts, $object, $args);

        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $object_output = $args->before;
        $object_output .= "<a {$attributes}>";
        $object_output .= $args->link_before . apply_filters('the_title', $object->title, $object->ID) . $args->link_after;
        if (str_contains($class_names, 'dropdown')) {
            $object_output .= ' </a><span class="js-item-toggle item-toggle"></span>';
        } else {
            $object_output .= ' </a>';
        }

        $object_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $object_output, $object, $depth, $args);
    }
}
