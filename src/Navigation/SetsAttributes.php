<?php

namespace Kindling\Support\Navigation;

trait SetsAttributes
{
    /**
     * Builds the link attributes.
     *
     * @param object $item
     * @param array $args
     * @return void
     */
    protected function buildLinkAttributes($item, $args)
    {
        dd($item);
        $atts = [];
        $atts['title'] = !empty($item->title) ? $item->title : '';
        $atts['target'] = !empty($item->target) ? $item->target : '';
        $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';
        $atts['href'] = !empty($item->url) ? $item->url : '';

        $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args);

        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        return $attributes;
    }

    /**
     * Get the item classes.
     *
     * @param WP_Post $item
     * @return void
     */
    protected function getClasses($item)
    {
        return collect(
            empty($item->classes) ? [] : (array)$item->classes
        );
    }

    /**
     * Sets the item classes.
     *
     * @param  WP_Post $item
     * @return array
     */
    protected function setClasses($item)
    {
        $classes = $this->getClasses($classes);
        $classes = $this->replaceActiveClasses($classes);
        $classes = $this->replaceDropdownClasses($classes);

        return $classes->unique()
        ->map('trim')
        ->reject(function ($value) {
            return str_contains($value, $this->unusedClasses());
        })
        ->filter()
        ->toArray();
    }

    /**
     * Gets extra unused classes.
     *
     * @return array
     */
    protected function unusedClasses()
    {
        return ['menu-item-', 'page_item', 'page-item'];
    }

    /**
     * Replaces the item "active" classes.
     *
     * @param \Illuminate\Support\Collection $classes
     * @return void
     */
    protected function replaceActiveClasses($classes)
    {
        return $this->replacesClasses($classes, [
            'current_page_item',
            'current-menu-ancestor',
            'current_page_ancestor',
            'current-menu-item',
            'current_page_parent',
            'current-menu-parent',
        ], 'active');
    }

    /**
     * Replaces the item "dropdown" classes.
     *
     * @param \Illuminate\Support\Collection $classes
     * @return void
     */
    protected function replaceDropdownClasses($classes)
    {
        return $this->replacesClasses($classes, [
            'menu-item-has-children',
        ], 'dropdown');
    }

    /**
     * Replaces an items classes.
     *
     * @param \Illuminate\Support\Collection $classes
     * @param array $needles
     * @param string $replacement
     * @return \Illuminate\Support\Collection
     */
    protected function replacesClasses($classes, $needles, $replacement)
    {
        if ($classes->intersect($needles)->count() > 0) {
            $classes->push($replacement);
        }

        return $classes->diff($needles);
    }
}
