<?php
/**
 * Kindling Support - Helpers.
 *
 * @package Kindling_Support
 *
*/

/**
 * Page titles.
 */
function kindling_page_title()
{
    if (is_home()) {
        if (get_option('page_for_posts', true)) {
            return get_the_title(get_option('page_for_posts', true));
        } else {
            return __('Latest Posts', 'kindling');
        }
    } elseif (is_archive()) {
        return get_the_archive_title();
    } elseif (is_search()) {
        return sprintf(__('Search Results for %s', 'kindling'), get_search_query());
    } elseif (is_404()) {
        return __('Not Found', 'kindling');
    } else {
        return get_the_title();
    }
}

/**
 * Kindling application container
 *
 * @param string $abstract
 *
 * @return mixed
 */
function kindling($abstract = null)
{
    static $container;

    if (!isset($container)) {
        $container = new Illuminate\Container\Container;
    }

    return $abstract ? $container->make($abstract) : $container;
}

if (!function_exists('r_collect')) {
    /**
     * Recursively collects a multi-dimensional array.
     *
     * @param  array $array
     *
     * @return Illuminate\Support\Collection
     */
    function r_collect($array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = r_collect($value);
                $array[$key] = $value;
            }
        }
        return collect($array);
    }
}

/**
 * Redirects
 *
 * @param string $path
 * @param integer $status
 * @return void
 */
function redirect($path, $status = 302)
{
    wp_redirect($path, 302);
    exit();
}

/**
 * Creates a URL.
 *
 * @param string $path
 * @return string
 */
function url($path = '')
{
    return network_home_url($path);
}

/**
 * Undocumented function
 *
 * @return WP_User
 */
function user()
{
    return wp_get_current_user();
}
