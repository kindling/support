<?php
/**
 * Kindling Support - Actions.
 *
 * @package Kindling_Support
 *
*/

if (!function_exists('add_action')) {
    return;
}

add_action('kindling_ready', function () {
    /**
     * Disable file editor.
     */
    if (!defined('DISALLOW_FILE_EDIT')) {
        define('DISALLOW_FILE_EDIT', !!apply_filters(
            'kindling_support_disable_file_edit',
            true
        ));
    }

    /**
     * Declares WooCommerce theme support
     */
    add_action('after_setup_theme', function () {
        add_theme_support('woocommerce');
    });

    /**
     * Filters Gravity Forms CSS classes for a field.
     *
     * @link    http://www.gravityhelp.com/documentation/page/Gform_field_css_class
     *
     * @param   string  $classes  The CSS classes to be filtered, separated by empty spaces (i.e. "gfield custom_class").
     * @param   array   $field    Current field.
     * @param   array   $form     Current form.
     *
     * @return  string            The filtered CSS classes separated by empty spaces (i.e. "gfield custom_class").
     */
    add_action('gform_field_css_class', function ($classes, $field, $form) {
        // Add the field label class.
        $label = (isset($field['label'])) ? $field['label'] : '';
        $admin_label = (isset($field['adminLabel'])) ? $field['adminLabel'] : '';
        $input_slug = ('' === $admin_label) ? $label : $admin_label;
        $input_slug = sanitize_title($input_slug);
        $classes .= " gfield-{$input_slug}";

        // Add the field type class.
        $type = (isset($field['type'])) ? $field['type'] : '';
        $type = sanitize_title($type);
        $classes .= " gfield-type-{$type}";

        // Add placeholder classes.
        $has_placeholder = (isset($field['placeholder']) and '' !== $field['placeholder']);
        if (isset($field['inputs']) and is_array($field['inputs'])) {
            foreach ($field['inputs'] as $input) {
                if (isset($input['placeholder']) and '' !== $input['placeholder']) {
                    $has_placeholder = true;

                    break;
                } // if()
            } // foreach()
        } // if()
        $classes .= ($has_placeholder) ? ' gfield-placeholder' : '';

        return $classes;
    }, 10, 3);

    /**
     * Theme setup.
     */
    add_action('after_setup_theme', function () {
        // Make theme available for translation
        // Community translations can be found at https://github.com/roots/sage-translations
        load_theme_textdomain('kindling', get_template_directory() . '/lang');

        // Enable plugins to manage the document title
        // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
        add_theme_support('title-tag');

        // Enable HTML5 markup support
        // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
        add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);
    });
});
